import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../book/models/Book';
import { BookService } from '../book/services/book.service';
import { ErrorHandlerService } from '../error-handler.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  id : number
  book : Book

  constructor(private router : Router, private route: ActivatedRoute, 
    private bookService: BookService, private errorHandler: ErrorHandlerService) { }

  ngOnInit(): void {
    this.getBookDetail()
  }

  // TODO: get book by id
  getBookDetail(){
    this.id = this.route.snapshot.params['id']
    this.bookService.getBookById(this.id).subscribe((data: Book) => {
        console.log(data)
        this.book = data;
      }, error => {
        this.errorHandler.handleError(error);
      })
  }

  // TODO: to nagivate bach to homepage
  backHome() {
    this.router.navigate(['/home'])
  }

}
