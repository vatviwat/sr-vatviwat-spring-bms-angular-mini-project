import { Category } from './../models/Category';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http : HttpClient) { }

  //TODO: get all categories
  getAllCategories(params): Observable<any>{
    return this.http.get(`http://localhost:8080/categories?page=${params}&size=3&sort=id,desc`)
  }

  //TODO: add new category
  createCategory(category: Category) {
    return this.http.post(`http://localhost:8080/categories`, category);
  }

  //TODO: delete category by id
  deleteCategory(id: number) {
    return this.http.delete(`http://localhost:8080/categories/${id}`, { responseType: 'text' });
  }

  //TODO: get category by id
  getCategoryById(id: number){
    return this.http.get(`http://localhost:8080/categories/${id}`)
  }

  //TODO: edit category by id
  editCategoryById(id: number, category: Category) {
    return this.http.put(`http://localhost:8080/categories/${id}`, category);
  }

}
