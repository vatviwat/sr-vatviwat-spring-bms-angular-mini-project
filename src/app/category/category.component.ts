import { Category } from './models/Category';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from './services/category.service';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NullTemplateVisitor } from '@angular/compiler';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
})
export class CategoryComponent implements OnInit {

  categories : Category[]

  //use for add new category
  category: Category = {} as Category;

  //store value of category detail
  categoryById: Category

  //use for edit category
  editCategory: Category = {} as Category;
  categoryEditById: Category

  //reactive form
  categoryForm = new FormGroup({
    title : new FormControl('',[Validators.required])
  })

  get f() { return this.categoryForm.controls; }

  page : number = 0;
  size : number = 3
  count : number
  tableSizes = [3, 6, 9, 12]

  constructor(private categoryService: CategoryService,
    config: NgbModalConfig, private modalService: NgbModal) {
      config.backdrop = 'static';
      config.keyboard = false;
  }

  ngOnInit(): void {
    this.getAllCategories()
  }

  //TODO: open modal to add a category
  open(content) {
    this.modalService.open(content)
  }

  //TODO: to close modal
  close(content : any){
    this.modalService.dismissAll(content)
    this.categoryForm.reset()
  }

  //TODO: get all categories
  getAllCategories(){
    this.categoryService.getAllCategories(this.page-1).subscribe((data:any) => {
      this.categories = data._embedded.categories
      this.count = data.page.totalElements
      console.log(this.categories);
    })
  }

  //TODO: change data in table on click page number
  onTableDataChange(event: number){
    this.page = event;
    this.getAllCategories();   
  }  

  //TODO: add new category
  onSubmit(){
    this.category = this.categoryForm.value
    this.categoryService.createCategory(this.category).subscribe((data:Category)=>{
      this.getAllCategories()
    })
    this.categoryForm.reset()
  }

  //TODO: delete category by id
  deleteCategory(id: number){
    this.categoryService.deleteCategory(id).subscribe((data:any)=>{
      this.getAllCategories();
    })
  }

  //TODO: get category by id
  getCategoryById(id: number){
    this.categoryService.getCategoryById(id).subscribe((data:Category)=>{
      this.categoryById = data
    })
  }

  //TODO: edit category by id
  editCategoryById(id: number){
    this.categoryService.getCategoryById(id).subscribe((data:Category)=>{
      this.categoryEditById = data
      this.categoryForm.get('title').setValue(this.categoryEditById.title)
    })
  }
  onEdit(){
    this.editCategory = this.categoryForm.value
    this.categoryService.editCategoryById(this.categoryEditById.id,this.editCategory).subscribe((data:Category)=>{
      this.getAllCategories()
    })
    this.categoryForm.reset()
  }

}
