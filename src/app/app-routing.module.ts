import { BookDetailComponent } from './book-detail/book-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book/book.component';
import { CategoryComponent } from './category/category.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InternalServerComponent } from './internal-server/internal-server.component';


const routes: Routes = [
  { path: 'home', component: BookComponent },
  { path: 'home/:id', component: BookDetailComponent },
  { path: 'categories', component: CategoryComponent },
  { path: 'error-404', component: PageNotFoundComponent },
  { path: 'error-500', component: InternalServerComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
