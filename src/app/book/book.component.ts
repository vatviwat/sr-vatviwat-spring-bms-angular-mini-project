import { Category } from './../category/models/Category';
import { Component, OnInit } from '@angular/core';
import { Book } from './models/Book';
import { BookService } from './services/book.service';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FileService } from './services/file.service';
import { __await } from 'tslib';
import { FileUpload } from './models/FileUpload';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent implements OnInit {

  books: Book[]
  searchBooks : Book[]
  isSearch : boolean = false
  isFilter : boolean = false
  search
  filter
  categories: Category[]

  book: Book = {} as Book

  //store value of category detail
  bookById: Book

  image: String
  selectedFile: FileUpload

  //use for edit category
  editBook: Book = {} as Book
  bookEditById: Book

  page : number = 0
  size : number = 3
  count : number
  tableSizes = [3, 6, 9, 12]


  //reactive form
  bookForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    author: new FormControl('', [Validators.required]),
    category: new FormGroup({
      id: new FormControl('', [Validators.required])
    })
  })

  constructor(private bookService: BookService,
    config: NgbModalConfig, private modalService: NgbModal, private fileService: FileService,
    private router: Router) {
    config.backdrop = 'static'
    config.keyboard = false
  }

  ngOnInit(): void {
    this.getAllBooks();
    this.getAllCategories();
  }

  //TODO: get form controls
  get f() {
    return this.bookForm.controls
  }

  //TODO: open modal to add a book
  openXl(content: any) {
    this.modalService.open(content, { size: 'xl' });
  }

  //TODO: open modal to add a category
  open(content: any) {
    this.modalService.open(content)
  }

  //TODO: close modal and clear form
  close(content: any) {
    this.modalService.dismissAll(content)
    this.image = null
    this.bookForm.reset()
  }

  //TODO: get all books
  getAllBooks() {
    this.bookService.getAllBooks(this.page-1).subscribe((data: any) => {
      this.books = data._embedded.books
      this.count = data.page.totalElements       
    })
  }

  //TODO: search book by title
  searchBookByTitle(event: any) {
    this.search = event.target.value
    if(event.target.value===""){
      this.isSearch = false  
      this.getAllBooks()
    }
    else{
      this.isSearch = true
      this.bookService.searchBookByTitle(this.search).subscribe((data: any) => {       
          this.page = 0
          this.books = data._embedded.books
          this.count = data.page.totalElements       
      })
    }
  }

  //TODO: search book by category title
  searchBookByCategoryTitle(event: any) { 
    this.filter = event.target.value
    if(this.filter===""){
      this.isFilter = false  
      this.getAllBooks()
    }
    else{
      this.isFilter = true
      this.bookService.searchBookByCategoryTitle(this.filter).subscribe((data: any) => {
        this.books = data._embedded.books
        this.count = data.page.totalElements
        this.page = 0
      })
    }
  }

  //TODO: change data in table on click page number
  onTableDataChange(event: number){
    if(this.isSearch==true){
      this.page = event
    }
    else if(this.isFilter==true){
      this.page = event
    }
    else{
      this.page = event
      this.getAllBooks(); 
    }
  }  

  //TODO: select file for upload
  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedFile = new FileUpload(event.target.result, file);
      this.selectedFile.pending = true;
      this.image = event.target.result
    });
    reader.readAsDataURL(file)

  }

  //TODO: add new book
  onSubmit = async () => {
    this.book = this.bookForm.value
    if (this.selectedFile != null) {
      const formData = new FormData();
      formData.append('files', this.selectedFile.file);
      await this.fileService.uploadImage(formData).toPromise().then((res: any) => {
        this.image = res.file1
        this.book.thumbnail = res.file1
      })
      await this.bookService.createBook(this.book).toPromise().then((data: Book) => {
        this.getAllBooks()
      })
    }
    else {
      this.book.thumbnail = 'https://sirencomms.com/wp-content/themes/massive-dynamic/assets/img/placeholders/placeholder1.jpg'
      await this.bookService.createBook(this.book).toPromise().then((data: Book) => {
        this.getAllBooks()
      })
    }
    this.bookForm.reset()
  }

  //TODO: get all categories
  getAllCategories() {
    this.bookService.getAllCategories().subscribe((data: any) => {
      this.categories = data._embedded.categories
    })
  }


  //TODO: get category by id
  getBookById(id: number){
    this.bookService.getBookById(id).subscribe((data:Book)=>{
      this.bookById = data
    })
  }

  //TODO: delete book by id
  deleteBook(id: number) {
    this.bookService.deleteBook(id).subscribe((data: any) => {
      if(this.isFilter){
        this.bookService.searchBookByCategoryTitle(this.filter).subscribe((data: any) => {
          this.books = data._embedded.books
          this.count = data.page.totalElements
          this.page = 0
        })
      }
      else if(this.isSearch){
        this.bookService.searchBookByTitle(this.search).subscribe((data: any) => {       
          this.page = 0
          this.books = data._embedded.books
          this.count = data.page.totalElements       
        })
      }
      else{
        if(data==null){
          this.page = 0
          this.getAllBooks()
        }
        else{
          this.getAllBooks()
        }
      }
    })
  }

  //TODO: edit book by id
  editBookById(id: number) {
    this.bookService.getBookById(id).subscribe((data: Book) => {
      this.bookEditById = data     
      this.bookForm.get('title').setValue(this.bookEditById.title)
      this.bookForm.get('description').setValue(this.bookEditById.description)
      this.image = this.bookEditById.thumbnail
      this.bookForm.get('author').setValue(this.bookEditById.author)
      this.bookForm.get('category.id').setValue(this.bookEditById.category.id)
    })
  }
  onEdit = async() =>{
    this.editBook = this.bookForm.value
    if(this.editBook.thumbnail!=null){
      await this.bookService.editBookById(this.bookEditById.id,this.editBook).toPromise().then((data: Book) => {
        this.getAllBooks()
      })
    }
    else{
      if(this.selectedFile != null) {
        const formData = new FormData();
        formData.append('files', this.selectedFile.file);
        await this.fileService.uploadImage(formData).toPromise().then((res: any) => {
          this.image = res.file1
          this.editBook.thumbnail = res.file1
        })
        await this.bookService.editBookById(this.bookEditById.id,this.editBook).toPromise().then((data: Book) => {
          if(this.isFilter){
            this.bookService.searchBookByCategoryTitle(this.filter).subscribe((data: any) => {
              this.books = data._embedded.books
              this.count = data.page.totalElements
              this.page = 0
            })
          }
          else if(this.search){
            this.bookService.searchBookByTitle(this.search).subscribe((data: any) => {       
              this.page = 0
              this.books = data._embedded.books
              this.count = data.page.totalElements       
            })
          }
          else{
            this.getAllBooks()
          }
        })
      }
      else {
        this.editBook.thumbnail = this.image
        await this.bookService.editBookById(this.bookEditById.id,this.editBook).toPromise().then((data: Book) => {
          if(this.isFilter){
            this.bookService.searchBookByCategoryTitle(this.filter).subscribe((data: any) => {
              this.books = data._embedded.books
              this.count = data.page.totalElements
              this.page = 0
            })
          }
          else if(this.search){
            this.bookService.searchBookByTitle(this.search).subscribe((data: any) => {       
              this.page = 0
              this.books = data._embedded.books
              this.count = data.page.totalElements       
            })
          }
          else{
            this.getAllBooks()
          }
        })
      }
    }
    this.bookForm.reset()
  }

  //TODO: navigate to book detail page
  bookDetail(id: number){
    this.router.navigate(['/home', id]);   
  }
}

