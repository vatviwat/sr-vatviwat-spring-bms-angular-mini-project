import { Book } from './../models/Book';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from 'src/app/category/models/Category';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  //TODO: get all books
  getAllBooks(params): Observable<any>{
    return this.http.get(`http://localhost:8080/books?page=${params}&size=3&sort=id,desc`)
  }

  //TODO: add new book
  createBook(book: Book) {
    return this.http.post(`http://localhost:8080/books`, book);
  }

  //TODO: get all categories
  getAllCategories(){
    return this.http.get(`http://localhost:8080/categories?sort=id`)
  }

  //TODO: delete book by id
  deleteBook(id: number) {
    return this.http.delete(`http://localhost:8080/books/${id}`, { responseType: 'text' });
  }

  //TODO: get category by id
  getBookById(id: number){
    return this.http.get(`http://localhost:8080/books/${id}`)
  }
  
  //TODO: edit category by id
  editBookById(id: number, book: Book) {
    return this.http.put(`http://localhost:8080/books/${id}`, book);
  }

  searchBookByTitle(params): Observable<any>{
    return this.http.get(`http://localhost:8080/books/search/bookTitle?title=${params}&sort=id,desc`, {params})
  }

  searchBookByCategoryTitle(params): Observable<any>{
    return this.http.get(`http://localhost:8080/books/search/bookCategory?title=${params}&sort=id,desc`, {params})
  }

}
