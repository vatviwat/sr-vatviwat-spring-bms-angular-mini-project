import { Category } from './../../category/models/Category';
export interface Book{
    id: number
    title: String
    description: String
    thumbnail: String
    author: String
    category: Category
}